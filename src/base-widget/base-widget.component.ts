import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: 'base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})

export class BaseWidgetComponent {

    public data: Array<any> = [
    {"related_organizations":"Afrikaans"},
    {"related_organizations":"العربية"},
    {"related_organizations":"Беларуская"},
    {"related_organizations":"Català"},
    {"related_organizations":"Deutsch"},
    {"related_organizations":"Español"},
    {"related_organizations":"Esperanto"},
    {"related_organizations":"Euskara"},
    {"related_organizations":"Français"},
    {"related_organizations":"हिन्दी"},
    {"related_organizations":"Hrvatski"},
    {"related_organizations":"Italiano"},
    {"related_organizations":"Lietuvių"},
    {"related_organizations":"Nederlands"},
    {"related_organizations":"日本語"},
    {"related_organizations":"Português"},
    {"related_organizations":"Română"},
    {"related_organizations":"Русский"},
    {"related_organizations":"Slovenščina"}];

}
